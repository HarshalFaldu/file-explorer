<?php

$currentPath = $_POST["newPath"];
$allNames = array();

if(strpos(realpath($currentPath),"storage") || strpos(realpath($_POST["selectedPath"]),"storage")){ //check if path is valid or not
if($_POST["flag"]=="move"){ // Move operation if user select move
    $oldPath = json_decode($_POST["selectedPath"],true);
    $newPath = $currentPath;
    for($i = 0;$i<count($oldPath);$i++){
        $lastPath = substr($oldPath[$i],0,-1);
        $fileName =  pathinfo($oldPath[$i], PATHINFO_BASENAME);    
        rename($lastPath, $newPath . $fileName.'/');
        array_push($allNames,$fileName);
    }
    $message="success";
}

if($_POST["flag"]=="copy"){// Copy operation if user select copy
    function recurse_copy($src,$dst){   // Check if there is folders or file inside a directory recursively and copy all the files
        global $allNames;
        if(is_dir($src)){                      
            if( !file_exists($dst) ){  // Create directory on new location if there is no directory of that name
                mkdir($dst,0700);  
                $name = pathinfo($dst, PATHINFO_BASENAME);
            }else{ // Check if file of that name is already exist or not
                $last = strripos($dst,"/");
                $update = substr($dst,0,$last);
                $name = file_newname($dst);
                $dst = $update.'/'.$name; // if exist then update the name with e.g harshal_1 
                mkdir($dst.'/',0777);
            }
            array_push($allNames, $name);
            $a = array_diff(scandir($src),array('.','..'));
            if(!empty($a)){
                foreach($a as $file){ // check if folders or files is present under that directory, if yes then again perform the operation
                    (is_dir($src.'/'.$file)) ? recurse_copy($src.'/'.$file,$dst.'/'.$file) : copy($src.'/'.$file,$dst.'/'.$file);
                }
            }
            return true;
        }else{
            if(file_exists($dst)){ // if it is file then copy a file, if file is already present then update its name
                $path1 = substr($dst,0,-1);
                $last = strripos($path1,"/");
                $update = substr($dst,0,$last)."/";
                $name = file_newname($dst);
                copy($src,$update.file_newname($dst));
            }else{ // if file not present then copy that file to new location
                copy($src,$dst);
                $name = pathinfo($dst, PATHINFO_BASENAME);
            }
            array_push($allNames, $name);
        }
    }

    function file_newname($path){ // Generate new name with added numeric value to make it unique
        $path1 = substr($path,0,-1);
        $last = strripos($path1,"/");
        $update = substr($path,0,$last)."/";
        $actual_name = pathinfo($path,PATHINFO_FILENAME);
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $i = 1;
        if(!empty($extension)){ // for files
            while(file_exists($update.$actual_name.".".$extension))
            {           
                $actual_name = (string)$actual_name.($i);
                $name = $actual_name.".".$extension;
                $i++;
            }
        }else{ // for folder
            $i = 1;
            while(file_exists($update.$actual_name))
            {     
                $actual_name = (string)$actual_name.($i);
                $name = $actual_name;
                $i++;
            }
        }
        
        return $name;
    }

    try {        
        $oldPath = json_decode($_POST["selectedPath"],true);
        $newPath = $currentPath;
        for($i = 0;$i<count($oldPath);$i++){ // loop through all the selected files to copy
            $lastPath = substr($oldPath[$i],0,-1);
            $fileName =  pathinfo($oldPath[$i], PATHINFO_BASENAME);    
            recurse_copy($lastPath,$newPath . $fileName );
        }        
        $message = "success";        
    } catch (\Throwable $th) {
        $message = "fail";
    }
}

$d['file'] = [];
$d['folder'] = [];
for($i = 0;$i<count($oldPath);$i++){
    $fileName =  $allNames[$i];
    $tmp = [
        'name' => $fileName,
        'path' => $newPath . $fileName.'/',
        'extension' => pathinfo($fileName, PATHINFO_EXTENSION)
    ];
    if(!is_dir($newPath . $fileName)){
       array_push($d['file'],$tmp);            
    }else{
        array_push($d['folder'],$tmp);
    }
}

$res['status'] = true;
$res['data'] = $d;
$res['message'] = $message;
}else{
    $res['status'] = true;
    $res['data'] = [];
    $res['message'] = "Enter proper path";
}
header("Content-Type:application/json");
echo json_encode($res);
?>