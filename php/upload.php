<?php

$target_dir = $_POST['currentPath'];
$ext = array("jpg","png","zip","jpeg","txt","xls","csv"); //Extensions that are accepted
$message = "";
$d = [];
for($i=0;$i<count($_FILES["file"]["name"]);$i++){
    if(check($_FILES["file"]["name"][$i]) == "upload_error1"){
        break;  // Check if there are any files of different formats
    }
}
if($message==""){  // If there is no error than upload the file in the specific location
    for($i=0;$i<count($_FILES["file"]["name"]);$i++){
        $target_file1 = $target_dir . basename($_FILES["file"]["name"][$i]);
        move_uploaded_file($_FILES["file"]["tmp_name"][$i],$target_file1);
        $temp = [
            'name' => basename($_FILES["file"]["name"][$i]),
            'path' => $target_file1.'/',
            'extension' => strtolower(pathinfo($target_file1,PATHINFO_EXTENSION))
        ];
        array_push($d,$temp);
    }
    $message = "Success";
}


function check($name){   // Function to validate the file
    global $target_dir,$ext,$upload_error,$uploadDone,$message;
    $upload_error = 0;
    $uploadDone = 0;
    $target_file = $target_dir . basename($name);
    $fileext = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    if(!in_array($fileext,$ext)){  // Check the extension and compare it with given extensions
        $upload_error = 1;
        $message = "Enter specific file(zip | png | jpg | JPEG | txt | xls)";
    }else{
        if(file_exists($target_file)){  // If extension is valid than check if file exist or not
            $upload_error = 1;
            $message = "File already exist";
        }
        else{
            $uploadDone = 1;
        }
    }
    return $upload_error==1? "upload_error".$upload_error : "uploadDone".$uploadDone;
}
$res['status'] = 'true';
$res['data'] = $d;
$res['message'] = $message;

echo(json_encode($res));
?>