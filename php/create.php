<?php
$d['file'] = [];
$d['folder'] = [];
if($_POST["type"]=="folder"){ // If user want to create folder
    $newPath = $_POST['path'].$_POST['foldername']."/";
    if(!is_dir($newPath)){
        mkdir($newPath);
        $message = "success";
        $temp = [
            'name' => $_POST['foldername'],
            'path' => $newPath,
            'extension' => pathinfo($_POST['foldername'], PATHINFO_EXTENSION)
        ];
        array_push($d['folder'],$temp);
    }else{
        $message = "fail";
    }
}
if($_POST["type"]=="file"){ // If user want to create file
    $newPath = $_POST['path'].$_POST['foldername'];
    if(!file_exists($newPath)){
        fopen($newPath,"w");
        $message = "success";
        $temp = [
            'name' => $_POST['foldername'],
            'path' => $newPath.'/',
            'extension' => pathinfo($_POST['foldername'], PATHINFO_EXTENSION)
        ];
        array_push($d['file'],$temp);
    }else{
        $message = "fail";
    }
}

$res['status'] = true;
$res['data'] = $d;
$res['message'] = $message;
header("Content-Type:application/json");
echo json_encode($res);
?>