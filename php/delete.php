<?php
$path = $_POST['path'];
function delete($path){
    $a = array_slice(scandir($path),2);
    foreach($a as $file){ // Check if the folder have folders or files inside or not. If there are recursive folder inside folder or files then call function recursively
        (is_dir($path.'/'.$file)) ? delete($path.'/'.$file) : unlink($path.'/'.$file); 
    }
    return rmdir($path);
}

$d = [];
try {
    for($i=0;$i<count($path);$i++){ // Loop for deleting N numbers of files or folders
        $fileName = pathinfo($path[$i], PATHINFO_BASENAME);
        is_dir($path[$i]) ? delete($path[$i]) : unlink(substr($path[$i],0,-1));
        $d[$i] = [
            'name' => $fileName
        ];
        $msg = "success";
    }
} catch (\Throwable $th) {
    $msg ="fail";    
};

$res['status'] = true;
$res['data'] = $d;
$res['message'] = $msg;
header("Content-Type:application/json");
echo json_encode($res);

?>