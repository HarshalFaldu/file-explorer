<?php
$path = $_POST['selectedPath'];
$name = $_POST['name'];
$message = "";
$d=[];

if(is_dir($path)){ // check name with pattern if there is a folder
    if(!preg_match("/^[a-zA-Z0-9]*$/", $name)){
        $message = "Enter proper folder name";
    }
}else{ // check name with pattern if there is a file
    if(!preg_match("/^.*\.(jpg|JPG|txt|TXT|png|PNG|jpeg|JPEG|xls|XLS|csv|CSV|zip|ZIP)$/", $name)){
        $message = "Enter proper file name";
    }
}

if($message!="Enter proper file name"){
    $path1 = substr($path,0,-1);
    $last = strripos($path1,"/");
    $update = substr($path,0,$last).'/'.$name;
    rename($path1,$update.'/'); // rename file with new name
    $message = "success";
    $d = [
        'name' => $_POST['name'],
        'path' => $update."/",
        'extension' => pathinfo($_POST['name'], PATHINFO_EXTENSION)
    ];
}
$res['status'] = true;
$res['data']  = $d;
$res['message'] = $message;
header("Content-Type:application/json");
echo json_encode($res);
?>