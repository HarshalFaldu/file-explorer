$(document).ready(function(){

    var dir_create = $(".left").html();
    dir_create = dir_create.replace("hiddenFolder","folder")
    var root = "../storage/";
    var operation;
    var path1 = [];
    $(".paste").prop("disabled",true);
    $(".paste").addClass("unselected");
    // check extension
    function set_ext(location,ext){
        ext = ext.toLowerCase();
        // console.log(location);
        switch(ext){
            case "txt":
                $(location).find("img").attr("src","./assets/images/txt.png");
                break;
            case "xls":
                $(location).find("img").attr("src","./assets/images/xls.png");
                break;
            case "png":
                $(location).find("img").attr("src","./assets/images/png.png");
                break;
            case "jpeg":
                $(location).find("img").attr("src","./assets/images/jpeg.png");
                break;
            case "jpg":
                $(location).find("img").attr("src","./assets/images/jpg.png");
                break;
            
        }
    }

    // append sublist on left tree
    $(document).on("click",".clickable",async function(){
        path =$(this).find("h2").html();
        if(!path.includes(".")){
            $(this).find("img").attr("src","./assets/images/openfolder.png")
            var level = $(this).parent();
            $(".menu").attr('data-val',$(this).parent().attr('data-val'));
            if($(this).parent().parent().hasClass("active")){
                $(this).parent().parent().removeClass("active"); 
                $(this).find("img").attr("src","./assets/images/foldr1.png")
                $(this).siblings(".sub").remove();
            }
            else{
                $.ajax({
                    url:"php/get_list.php",
                    dataType:'json',
                    method:"POST",
                    data:{foldername:$(level).attr('data-val'),path:$(level).attr('data-val')},
                    success:function(res){
                        for(var i=0;i<res[0].length;i++){
                            if(dir_create.includes('folder')==true){
                                dir_create = dir_create.replace("folder","sub");
                            }
                            $(level).append(dir_create);
                            $(level).children(".sub").eq(i).find("h2").html(res[0][i]);
                            $(level).children('.sub').eq(i).children().attr('data-val',res[1][i]);
                            set_ext($(level).children().eq(i+1),res[2][i]);
                        }
                    }
                })
                $(this).parent().parent().addClass("active");
                $(this).closest(".folder").siblings().find(".sub").remove();
            }  
            $(".menu").children().remove();
            get_list($(".menu").attr('data-val'));
            }
    })


    // Default add list on left tree
    add_list_tree(root)
    function add_list_tree(root){
        $.ajax({
            url:"php/get_list.php",
            dataType:'json',
            method:"POST",
            data:{foldername:root,path:root},
            success:function(res){
                console.log(res);
                for(var i=0;i<res[0].length;i++){
                    $(".left").append(dir_create);
                    $(".folder:eq("+i+")").find("h2").html(res[0][i]);
                    $(".folder:eq("+i+")").find(".acc-title").attr('data-val',res[1][i]);   
                    set_ext($(".folder:eq("+i+")"),res[2][i]);
                }
            }
        })
    }
    
    var folder_clone = $(".menu_hidden").html();
    folder_clone = folder_clone.replace("hidden ","")
    get_list(root)
    
    function get_list(path1){
        $.ajax({
            url:"php/get_list.php",
            dataType:'json',
            method:"POST",
            data:{foldername:path1,path:path1},
            success:function(res){
                for(var i=0;i<res[0].length;i++){
                    $(".menu").append(folder_clone);
                    $(".folders:eq("+(i+1)+")").find("span").html(res[0][i]);
                    $(".folders:eq("+(i+1)+")").find(".fdir").attr('data-val',res[1][i]);   
                    set_ext( $(".folders:eq("+(i+1)+")"),res[2][i]);
                }
            }
        })
    }

    // Single click on folder right side
    $(document).on("click",".main_folder",function(event){
        if(!event.ctrlKey){
            $(this).addClass("blue_active").closest(".folders").siblings().find(".main_folder").removeClass('blue_active');
        }else{
            $(this).addClass("blue_active");
        }
    })
    
    // Double click on folder Right side
    $(document).on("dblclick",".main_folder",function(){
        
        var type = $(this).find("span").html();
        if(!type.includes(".")){
            var at = $(this).parent().attr('data-val');
            $(".menu").attr("data-val",at);
            $(this).closest(".menu").children().remove();
            $.ajax({
                url:"php/get_list.php",
                    dataType:'json',
                    method:"POST",
                    data:{foldername:$(".menu").attr("data-val"),path:$(".menu").attr("data-val")},
                    success:function(res){
                        for(var i=0;i<res[0].length;i++){
                            $(".menu").append(folder_clone);
                            $(".menu").children(".folders").eq(i).find("span").html(res[0][i]);
                            $(".menu").children(".folders").eq(i).find(".fdir").attr('data-val',res[1][i]);   
                            set_ext($(".menu").children().eq(i),res[2][i]);
                        }
                    }
            })
        }
    })

    // Back button
    $(document).on("click",".back",function(){
        var oldPath = $(".menu").attr("data-val");
        $(".menu").children().remove();
        $(".menu").attr("data-val" , oldPath.substring( 0 , oldPath.lastIndexOf('/' ,  oldPath.lastIndexOf('/')-1 ) + 1 ) )
        if($(".menu").attr("data-val")=="../" ){
            bootbox.alert({
                message: "You cannot go back",
                backdrop: true
            });
            $(".menu").attr("data-val","../storage/");
            get_list($(".menu").attr("data-val"));
        }else{
            get_list($(".menu").attr("data-val"));
        }
    })
    // Create Folder
    $(document).on("click",".create",function(){
        var currentPath = $(".menu").attr("data-val");
        var patt = new RegExp("^[a-zA-Z0-9]*$");
        bootbox.prompt({
            title: "Insert name of the folder.", 
            centerVertical: true,
            callback: function(result){
                if(patt.test(result) && result != null){
                    $.ajax({
                        url:"php/create.php",
                        method:"POST",
                        dataType:"json",
                        data:{foldername:result,path:currentPath,type : "folder"},
                        success:function(res){
                            if(res=="success"){
                                bootbox.alert({
                                    message: "Folder created",
                                    backdrop: true
                                });
                                // console.log($(".folder").hasClass("active"));
                                if(!$(".folder").hasClass("active")){
                                //     $(".active").find("clickable").siblings().remove();
                                //     add_list_tree($(".active").find(".acc-title").attr('data-val'));
                                // }else{
                                    $(".menu").children().remove();
                                    get_list($(".menu").attr("data-val"));
                                    $(".left").children().not($(".left").children().eq(0)).remove();
                                    add_list_tree($(".menu").attr("data-val"));
                                }
                            }else{
                                bootbox.alert({
                                    message: "Error while creating folder",
                                    backdrop: true
                                });
                            }
                        }
                    })
                }
            }
        });

    })

    // Deleting a directory
    $(document).on("click",".delete",async function(){
        var selected = $(".blue_active");
        var path1=[],index = [];
        for(var i=0;i<selected.length;i++){
            path1[i] = selected.eq(i).parent().attr('data-val');
            index[i] = selected.eq(i).closest(".folders").index();
        }
        await bootbox.confirm("Confirm you have to delete directory!",async function(result){ 
            if(result==true){
                await $.ajax({
                    url:"php/delete.php",
                    method:"POST",
                    dataType:"json",
                    data:{path:path1},
                    success:function(res){
                        if(res == "success"){
                            for(var i=0;i<selected.length;i++){
                                selected.eq(i).closest('.folders').remove();
                                var att = selected.eq(i).parent().attr("data-val");
                                $('[data-val="'+att+'"]').parent().remove();
                            }
                        }else{
                            bootbox.alert({
                                message: "Error while deleting folder",
                                backdrop: true

                            });
                        }
                    }
                })
            }
        });
    })

    // Add a file
    $(document).on("click",".addfile",function(){
        var currentPath = $(".menu").attr("data-val");
        var patt = new RegExp("^.*\.(jpg|JPG|txt|TXT|png|PNG|jpeg|JPEG|xls|XLS)$");
        bootbox.prompt({
            title: "Insert name of the File.", 
            centerVertical: true,
            callback: function(result){
                if(patt.test(result)){
                    $.ajax({
                        url:"php/create.php",
                        method:"POST",
                        dataType:"json",
                        data:{foldername:result,path:currentPath,type :"file"},
                        success:function(res){
                            if(res=="success"){
                                bootbox.alert({
                                    message: "Folder created",
                                    backdrop: true
                                });
                                // if(!$(".folder").hasClass("active")){
                                //     $(".active").find("clickable").siblings().remove();
                                //     add_list_tree($(".active").find(".acc-title").attr('data-val'));
                                // }else{
                                    $(".menu").children().remove();
                                    get_list($(".menu").attr("data-val"));
                                    // $(".folder").find($('[data-val="'+$(".menu").attr("data-val")+'"]')).children().eq(0).siblings().remove()
                                    // add_list_tree($(".menu").attr("data-val"));
                                // }
                            }else{
                                bootbox.alert({
                                    message: "Error while creating folder",
                                    backdrop: true
                                });
                            }
                        }
                    })
                }else{
                    bootbox.alert({
                        message: "File must be png,jpg,jpeg,txt and xls",
                        backdrop: true
                    });
                }
            }
        })
    })

    $(document).on("click",".move",function(){
        $(".paste").prop("disabled",false);
        $(".paste").removeClass("unselected");
        var selected = $(".blue_active");
        for(var i=0;i<selected.length;i++){
            path1[i] = selected.eq(i).parent().attr('data-val');
        }
        operation = "move";
    })

    $(document).on("click",".paste",function(){
        var currentPath = $(".menu").attr("data-val");    
        $.ajax({
            url:"php/move.php",
            method:"POST",
            dataType:"json",
            data:{selectedPath:JSON.stringify(path1),newPath:currentPath,flag:operation,type :"paste"},
            success:function(res){
                if(res=="success"){
                    bootbox.alert({
                        message: "Folder created",
                        backdrop: true
                    });
                    $(".menu").children().remove();
                    get_list(currentPath);
                }else{
                    bootbox.alert({
                        message: "Error while creating folder",
                        backdrop: true
                    });
                }
                $(".paste").addClass("unselected");
                $(".paste").prop("disabled",true);
            }
        })
    })

    $(document).on("click",".copy",function(){
        operation = "copy";
        var selected = $(".blue_active");
        for(var i=0;i<selected.length;i++){
            path1[i] = selected.eq(i).parent().attr('data-val');
        }
        if(selected.length>0){
            $(".paste").prop("disabled",false);
            $(".paste").removeClass("unselected");
        }
    })
    
    function rename(){
        var updatedPath = $(".blue_active").parent().attr("data-val");
        bootbox.prompt({
            title: "Insert name of the File.", 
            centerVertical: true,
            callback: function(result){
                if(result==true){
                    $.ajax({
                        url:"php/rename.php",
                        method:"POST",
                        dataType:"json",
                        data:{selectedPath:updatedPath,name:result},
                        success:function(res){
                            if(res[0]=="success"){
                                bootbox.alert({
                                    message: "Name Updated",
                                    backdrop: true
                                });
                                $(".blue_active").parent().attr("data-val",res[1]);
                                $(".blue_active").find("scan").html(result);
                                $(".menu").children().remove();
                                get_list($(".menu").attr("data-val"));
                            }else{
                                bootbox.alert({
                                    message: "Error while Renaming",
                                    backdrop: true
                                });
                            }
                        }
                    })
                }
            }
        
    })

    }

    $(document).bind("contextmenu", function (event) {
        event.preventDefault();
        if($(".fdir").find(event.target).length>0){
           $(event.target).closest(".main_folder").addClass("blue_active");
            $(".custom-menu").finish().toggle(100).
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }else{
            $(".custom-menu-2").finish().toggle(100).
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }
    });

    $(document).bind("mousedown", function (e) {
        if (!$(e.target).parents(".custom-menu").length > 0) {
            $(".custom-menu").hide(100);
        }
        if (!$(e.target).parents(".custom-menu-2").length > 0) {
            $(".custom-menu-2").hide(100);
        }
    });

    $(".custom-menu li").click(function(){
        switch($(this).attr("data-action")) {
            case "Cut": 
                $(".move").trigger("click");
                break;
            case "Copy": 
                $(".copy").trigger("click"); 
                break;
            case "Delete": 
                $(".delete").trigger("click"); 
                break;
            case "Rename": 
                // $(".rename").trigger("click");  
                rename();
                break;
        }
        $(".custom-menu").hide(100);
      });

    $(".custom-menu-2 li").click(function(){
    switch($(this).attr("data-action")) {
        case "Create File": 
            $(".addfile").trigger("click"); 
            break;
        case "Create Folder": 
            $(".create").trigger("click");  
            break;
        
    }
    $(".custom-menu").hide(100);
    $(".custom-menu-2").hide(100);
    });
})