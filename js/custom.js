$(document).ready(function(){
var root = "../storage/";
var operation, path1 = [];
$(".paste").prop("disabled",true).addClass("unselected");

// set Image as per extension
function set_ext(location,ext){
    $(location).find(".image,.rightimage").css("background-image", `url("./assets/images/${ext}.png")`)
}

// append sublist on left tree
$(document).on("click",".clickable",async function(){
    path =$(this).find("h2").html();
    if(!path.includes(".")){
        var level = $(this).parent();
        $(".menu,.left").attr('data-val',$(this).parent().attr('data-val'));
        if($(this).parent().parent().hasClass("active") ){ // remove active class if it is already open and remove their childrens
            $(this).parent().parent().removeClass("active"); 
            $(this).siblings(".sub").remove();
        }
        else{ // add active class and open their childrens 
            $(this).parent().parent().addClass("active").siblings().removeClass('active');
            $(this).closest(".folder").siblings().find(".sub").remove();
            $(".menu").children().remove();
            addDataCall($(".menu").attr('data-val'),level,".menu","left_tree");
        }  
    }
})

// Default add list on left tree and right menu
addDataCall(root,".left",".menu","default");
function addDataCall(root,leftLocation,rightLocation,call){
    $.ajax({
        url:"php/get_list.php",
        dataType:'json',
        method:"POST",
        data:{foldername:root,path:root},
        success:function(res){
            if(res['message']=="Success"){
                $.each(res['data'],function(key1,val){
                    $.each(val,function(key,val){                 
                        addData(val,leftLocation,rightLocation,call) // Add values both side
                    })
                });
            }else{
                bootbox.alert({message:res['message']});
            }
        }
    })
}

// Function Add the values on the selected path both side
function addData(val,leftLocation,rightLocation,call){
    var rightDivAppend = $(".hiddenFolder").clone();
    call == "left_tree" ? rightDivAppend.removeClass("hiddenFolder").addClass("sub") : rightDivAppend.removeClass("hiddenFolder").addClass("folder")
    var leftDivAppend = $(".menu_hidden").clone();
    leftDivAppend = leftDivAppend.children().removeClass("hidden")
    rightDivAppend.find("h2").html(val['name']);
    rightDivAppend.find(".acc-title").attr('data-val',val['path']);  
    leftDivAppend.find("span").html(val['name']);
    leftDivAppend.find(".fdir").attr('data-val',val['path']);   
    if(val['extension']!=""){
        set_ext(rightDivAppend,val['extension']);
        set_ext(leftDivAppend,val['extension']);
    } 
    $(leftLocation).append(rightDivAppend); // append child on left side
    $(rightLocation).append(leftDivAppend); // append child on right side
}

// Double click on folder right side
$(document).on("dblclick",".main_folder",function(){    
    var type = $(this).find("span").html();
    if(!type.includes(".")){
        var at = $(this).parent().attr('data-val');
        $(".menu,.left").attr("data-val",at);
        $(this).closest(".menu").children().remove();
        $(".left").find('[data-val="'+$(".menu").attr("data-val")+'"]').parent().addClass('active') //add active class at left side in perticular ID
        addDataCall($(".menu").attr("data-val"),$(".left").find('[data-val="'+$(".menu").attr("data-val")+'"]'),".menu","left_tree"); // append childs on left side and show files of that directories on right side
    }
})

// Single click on folder right side which selects the folder
$(document).on("click",".main_folder",function(event){
    event.ctrlKey ?  $(this).addClass("blue_active") : $(this).addClass("blue_active").closest(".folders").siblings().find(".main_folder").removeClass('blue_active');
})

// Back functionality
$(document).on("click",".back",function(){
    var oldPath = $(".menu").attr("data-val");
    if(oldPath==root ){
        bootbox.alert({message: "You cannot go back"});  
    }else{
        $(".menu").children().remove(); //remove last name from the path and get the files or new path and show it
        $(".menu,.left").attr("data-val" , oldPath.substring( 0 , oldPath.lastIndexOf('/' ,  oldPath.lastIndexOf('/')-1 ) + 1 ) )
        addDataCall($(".menu,.left").attr("data-val"),"",".menu","default");
        $(".menu,.left").attr("data-val") == "../storage/" ? $('.left').find(".active").find('.sub').remove() : $('.left').find('[data-val="'+$(".menu").attr("data-val")+'"]').find(".active").find('.sub').remove()
        $(".left").attr("data-val")=="../storage/" ? $('.folder').removeClass("active") : "";
        $('.left').find('[data-val="'+$(".menu").attr("data-val")+'"]').parent().hasClass("active") ? $('.left').find('[data-val="'+$(".menu").attr("data-val")+'"]').children().removeClass("active") : "";
    }
})

// Function creates file or folder as per the call
function create_file_folder(type){
    var currentPath = $(".menu").attr("data-val");
    var pattern = $(type).hasClass("addfile") ? new RegExp("^.*\.(jpg|JPG|txt|TXT|png|PNG|jpeg|JPEG|xls|XLS|csv|CSV|zip|ZIP)$") : new RegExp("^[a-zA-Z0-9]*$");
    bootbox.prompt({
        title: "Insert name of the folder/file.", 
        centerVertical: true,
        callback: function(result){
            if(result!=null){
                var regexResult = pattern.test(result);
                $(type).hasClass("addfile") ? typeOfOperation = "file" : typeOfOperation = "folder";
                if(regexResult){
                    $.ajax({
                        url:"php/create.php",
                        method:"POST",
                        dataType:"json",
                        data:{foldername:result,path:currentPath,type : typeOfOperation},
                        success:function(res){
                            if(res['message']=="success"){ // Showing data without call API again
                                res['data']['file'].length>0 ? renderData(res['data']['file'][0],currentPath) : renderData(res['data']['folder'][0],currentPath)
                            }else{
                                bootbox.alert({message: "Error while creating file/folder"});
                            }
                        }
                    })
                }else{
                   bootbox.alert({message:"Write proper name"}) ;
                }
            }
        }
    });
}

// Create file and folder
$(document).on("click",".create,.addfile",function(){
    create_file_folder($(this));
})

// Delete functionality
$(document).on("click",".delete", function(){
    var selected = $(".blue_active");
    selected.length>0 ? flag = true : flag = false;
    var path1=[],index = [];
    for(var i=0;i<selected.length;i++){ // Select all files which we want to delete and get their paths
        path1[i] = selected.eq(i).parent().attr('data-val');
        index[i] = selected.eq(i).closest(".folders").index();
    }
    if(selected.length>0){
        bootbox.confirm("Confirm you have to delete directory!", function(result){ 
            if(result==true && flag==true){
                $.ajax({
                    url:"php/delete.php",
                    method:"POST",
                    dataType:"json",
                    data:{path:path1},
                    success:function(res){
                        if(res['message'] == "success"){
                            for(var i=0;i<selected.length;i++){ // Delete those files from the window
                                selected.eq(i).closest('.folders').remove();
                                var att = selected.eq(i).parent().attr("data-val");
                                $(".left").find('[data-val="'+att+'"]').parent().remove();
                            }
                        }else{
                            bootbox.alert({message: "Error while deleting folder"});
                        }
                    }
                })
            }else{bootbox.alert({message: "No file is selected"});}
        });          
    }
})

// Directly go to home functionality
$(document).on("click",".home",function(){
    if($('.menu').attr("data-val")!=root){
        $(".menu").children().remove();
        $(".left").children().not(":eq(0)").remove();
        addDataCall(root,$(".left"),$(".menu"),"default")
    }
})

// Select files for move or copy
$(document).on("click",".move,.copy",function(){
    var selected = $(".blue_active");
    for(var i=0;i<selected.length;i++){
        path1[i] = selected.eq(i).parent().attr('data-val');
        $(this).hasClass("move") ? $(".left").find('[data-val="'+path1[i]+'"]').parent().remove() : "";
    }
    selected.length>0 ? $(".paste").prop("disabled",false).removeClass("unselected"):"";
    $(this).hasClass("move") ? operation = "move" : operation = "copy"; //Select type of operation
})

// Paste functionality
$(document).on("click",".paste",function(){
    var currentPath = $(".menu").attr("data-val");    
    $.ajax({
        url:"php/move.php",
        method:"POST",
        dataType:"json",
        data:{selectedPath:JSON.stringify(path1),newPath:currentPath,flag:operation,type :"paste"},
        success:function(res){
            if(res['message']=="success"){
                bootbox.alert({message: "Folder created"});
                $.each(res['data'],function(key1,val){
                    $.each(val,function(key,val1){
                        renderData(val1,currentPath)
                    })
                })
            }else{
                bootbox.alert({message: "Error while creating folder"});
            }
            $(".paste").prop("disabled",true).addClass("unselected");
        }
    })
})

//Add file or folder without API call
function renderData(value,currentPath){
    if(currentPath.includes("../storage")==true && currentPath == "../storage/"){
        addData(value,$('.left'),$('.menu'),"default")
    }else{
        addData(value,$('.left').find('[data-val="'+currentPath+'"]').parent(),$('.menu'),"left_tree")
    }   
}

// Rename Functionality
$(document).on("click",".rename",function(){
    rename();
})

function rename(){
    var updatedPath = $(".blue_active").parent().attr("data-val");
    if(updatedPath!=undefined){
        bootbox.prompt({
            title: "Insert name of the File.", 
            centerVertical: true,
            callback: function(result){
                if(result!=null){
                    $.ajax({
                        url:"php/rename.php",
                        method:"POST",
                        dataType:"json",
                        data:{selectedPath:updatedPath,name:result},
                        success:function(res){
                            if(res['message']=="success"){ // Change the name of file or folder
                                $('.left').find('[data-val="'+updatedPath+'"]').find("h2").html(res['data']['name']);
                                $(".blue_active").parent().attr("data-val",res['data']['path']);
                                $(".blue_active").find("span").html(res['data']['name']);
                            }else{
                                bootbox.alert({message: "Error while Renaming"});
                            }
                        }
                    })
                }else{
                    bootbox.alert({message:"Enter name"});
                }
            }  
        })
    }
}

// Context menu
    $(document).bind("contextmenu", function (event) {
        event.preventDefault();
        if($(".fdir").find(event.target).length>0){
        $(event.target).closest(".main_folder").addClass("blue_active");
            $(".custom-menu").finish().toggle(100).
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }else{
            $(".custom-menu-2").finish().toggle(100).
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }
    });

    $(document).bind("mousedown", function (e) {
        if (!$(e.target).parents(".custom-menu,.custom-menu-2").length > 0) {
            $(this) == ".custom-menu" ? $(".custom-menu").hide(100) : $(".custom-menu-2").hide(100);
        }
    });

    $(".custom-menu li").click(function(){
        switch($(this).attr("data-action")) {
            case "Cut": 
                $(".move").trigger("click");
                break;
            case "Copy": 
                $(".copy").trigger("click"); 
                break;
            case "Delete": 
                $(".delete").trigger("click"); 
                break;
            case "Rename": 
                rename();
                break;
        }
        $(".custom-menu").hide(100);
    });

    $(".custom-menu-2 li").click(function(){
        switch($(this).attr("data-action")) {
            case "Create File": 
                $(".addfile").trigger("click"); 
                break;
            case "Create Folder": 
                $(".create").trigger("click");  
                break;  
        }
        $(".custom-menu,.custom-menu-2").hide(100);
    });

    // Upload functionality
    var myDropjs,currentPath;
    $(document).on("click",".upload",function(){
        currentPath = $(".menu").attr("data-val");
        dialogue = bootbox.dialog({  // Open the bootbox to upload files
            title: 'Custom Dialog Example',
            message: `<form enctype="multipart/form-data" method="POST"><div id="my-dropzone" class="dropzone" name="mainFileUploader"><div class="fallback"><input type="file" name="filetoupload[]" id="filetoupload" class="fl w60" multiple></div></div></form><div><button type="submit" id="submit-all"> upload </button></div>`,
            centerVertical : true,
            size: 'large',
            onEscape: true,
            backdrop: true,   
        }).on('shown.bs.modal',function(){ // Trigger when bootbox is open
            Dropzone.autoDiscover = false;
            myDropjs = new Dropzone('.dropzone',{
                url: "php/upload.php", // Call the API to upload
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,
                addRemoveLinks:true,
                acceptedFiles: '.jpg,.jpeg,.txt,.zip,.png,.csv,.xls', // Limitations of file extensions
                init: function () {
                    this.on('sendingmultiple', function (data, xhr, formData) {
                        formData.append("currentPath", currentPath);    // Append current path to the output data
                    });
                }
            });
        })
    })

    $(document).on("click","#submit-all",function(){  // trigger when click on Upload button
        if(myDropjs.getRejectedFiles().length==0){  // Check weather any file with extension other than selected extensions 
            myDropjs.processQueue();
            myDropjs.on('successmultiple',function(file,res){
                var abc = JSON.parse(res)
                if(abc['message']=="Success"){  // Append the data
                    $.each(abc['data'],function(key1,val){
                        renderData(val,currentPath);
                    })      
                }else{
                    bootbox.alert({message: abc['message']});
                }
                dialogue.modal("hide"); // Close the boot box
            }); 
        }else{
            bootbox.alert({message:"Remove files other than CSV, ZIP, JPG, JPEG, TXT, XLS"});
        }
    })


    // $(document).on("click",".submit",function(){
    //     var currentPath = $(".menu").attr("data-val");        
    //     var fd = new FormData($('#form')[0]);
    //     fd.append("currentPath",currentPath);    
    //        $.ajax({
    //         url:"php/upload.php",
    //         method:"POST",
    //         data:fd,
    //         processData:false,
    //         contentType: false,
    //         dataType:"json",
    //         success:function(res){
    //             console.log(res);
    //             if(res['message']=="Success"){
    //                 $.each(res['data'],function(key1,val){
    //                     renderData(val,currentPath);
    //                 })
    //             }else{
    //                 bootbox.alert({message: res.message});
    //             }
    //         }
    //     })
    //     dialogue.modal("hide");
    // })
})